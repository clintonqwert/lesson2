//
//  NewTableViewCell.swift
//  Lesson2
//
//  Created by Clinton Jay Ramonida on 2017-07-12.
//  Copyright © 2017 Clinton Jay Ramonida. All rights reserved.
//

import UIKit

class NewTableViewCell: UITableViewCell {
    @IBOutlet weak var productCountLabel: UILabel!

    @IBOutlet weak var newCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
