//
//  ProductTableViewController.swift
//  Lesson2
//
//  Created by Clinton Jay Ramonida on 2017-07-12.
//  Copyright © 2017 Clinton Jay Ramonida. All rights reserved.
//

import UIKit

class ProductTableViewController: UITableViewController {

    var productArray = [ProductItem]()
    
    var category : ProductCategory!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
        
        getProductData()
    }
    
    private func getProductData () {
        
        guard let categoryId = category.categoryId else {
            return
        }
        
        let myUrl = URL(string: "http://www.bestbuy.ca/api/v2/json/search?categoryid=\(categoryId)&lang=en")!
        let myRequest = URLRequest(url: myUrl)
        let session = URLSession.shared
        let task = session.dataTask(with: myRequest) {
            (data, response, error) -> Void in
            if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []), let dict = json as? [String: Any] {
                if let subProductsArray = dict["products"] as? [[String: Any]] {
                    
                    for products in subProductsArray {
                        
                        if let name = products["name"] as? String,
                            let customerRating = products["customerRating"] as? Float,
                            let regularPrice = products["regularPrice"] as? Float {
                            
                            let product = ProductItem(name: name, regularPrice: regularPrice, customerRating: customerRating)
                            
                            self.productArray.append(product)
                            
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                }
            }
        }
        task.resume()
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return productArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductTableViewCell
        
        cell.productLabel.text = productArray[indexPath.row].name
        cell.customerRatingLabel.text = "Customer Rating: "
        cell.ratingLabel.text = "\(productArray[indexPath.row].customerRating!)"
        cell.regularPriceLabel.text = "\(productArray[indexPath.row].regularPrice!)"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
