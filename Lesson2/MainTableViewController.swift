//
//  MainTableViewController.swift
//  Lesson2
//
//  Created by Clinton Jay Ramonida on 2017-07-05.
//  Copyright © 2017 Clinton Jay Ramonida. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {
    
    var classmates: [ClassMate]!
    
    var categories	= [String]()
    
    
    func loadCategoriesData()	{
        categories.append("Computers")
        categories.append("Tables")
        categories.append("Washing Machines")
        categories.append("Games")
        categories.append("Headphones")
        
    }
    
    /*
    func loadClassmateData()	{
        classmates.append("Clinton")
        classmates.append("Hilaria")
        classmates.append("Jaxon")
        classmates.append("Ross")
        classmates.append("Matias")
        
    }*/

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadCategoriesData()
        //self.loadClassmateData()
        
        classmates = [ClassMate]()
        classmates.append(ClassMate(name: "Hillary", country: "Venezuela"))
        classmates.append(ClassMate(name: "Neo", country: "Thailand"))
        classmates.append(ClassMate(name: "Jesse", country: "Canada"))

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "categoryCells")
        
        tableView.register(UINib(nibName: "ClassmateTableViewCell", bundle: nil), forCellReuseIdentifier: "classmateCells")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return categories.count
        }
        return classmates.count
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCells", for: indexPath) as! TableViewCell
            
                let myCategories = categories[indexPath.row]
                cell.cellLabel.text = myCategories
                return cell
            }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "classmateCells", for: indexPath) as! ClassmateTableViewCell
        
        let classmate = classmates[indexPath.row]
        cell.setupCell(withClassmate: classmate)
        return cell
            

        //let cell = UITableViewCell()
        
        //cell.textLabel?.text = "Clinton"
        
        // Configure the cell...
        //cell.textLabel?.text = "Test"

    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 44
        }
        return 80
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
