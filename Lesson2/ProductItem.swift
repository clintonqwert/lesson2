//
//  ProductItem.swift
//  Lesson2
//
//  Created by Clinton Jay Ramonida on 2017-07-19.
//  Copyright © 2017 Clinton Jay Ramonida. All rights reserved.
//

import Foundation

class ProductItem {
    
    let name : String!
    let regularPrice: Float!
    let customerRating: Float!
    
    init(name : String, regularPrice: Float, customerRating: Float) {
        
        self.name = name
        self.regularPrice = regularPrice
        self.customerRating = customerRating
    }
}
