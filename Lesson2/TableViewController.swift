//
//  TableViewController.swift
//  Lesson2
//
//  Created by Clinton Jay Ramonida on 2017-07-12.
//  Copyright © 2017 Clinton Jay Ramonida. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    var categoryArray = [ProductCategory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "NewTableViewCell", bundle: nil), forCellReuseIdentifier: "NewCell")
        
        getNetworkData()
    }
    
    private func getNetworkData () {
        
        let myUrl = URL(string: "http://www.bestbuy.ca/api/v2/json/category/departments?lang=en&format=json")!
        let myRequest = URLRequest(url: myUrl)
        let session = URLSession.shared
        let task = session.dataTask(with: myRequest) {
            (data, response, error) -> Void in
            if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []), let dict = json as? [String: Any] {
                if let subCategoriesArray = dict["subCategories"] as? [[String: Any]] {
                    
                    for subCategory in subCategoriesArray {
                        
                        if let categoryId = subCategory["id"] as? String,
                            let name = subCategory["name"] as? String,
                            let hasSubcategories = subCategory["hasSubcategories"] as? Bool,
                            let productCount = subCategory["productCount"] as? Int {
                            
                            let category = ProductCategory(categoryId: categoryId, name: name, hasSubcategories: hasSubcategories, productCount: productCount)
                            
                            self.categoryArray.append(category)
                            
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                }
            }
        }
        task.resume()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return categoryArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewCell", for: indexPath) as! NewTableViewCell
        
        cell.newCell.text = categoryArray[indexPath.row].name
        cell.productCountLabel.text = "\(categoryArray[indexPath.row].productCount!)"

        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = categoryArray[indexPath.row]
        
        let tc = ProductTableViewController()
        tc.category = category
        navigationController?.pushViewController(tc, animated: true)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
