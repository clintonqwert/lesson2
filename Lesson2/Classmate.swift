//
//  Classmate.swift
//  Lesson2
//
//  Created by Clinton Jay Ramonida on 2017-07-05.
//  Copyright © 2017 Clinton Jay Ramonida. All rights reserved.
//

import Foundation

class ClassMate {
    
    let name: String!
    let country: String!
    
    init(name:String, country: String){
        self.name = name
        self.country = country
    }
}
