//
//  ProductCategory.swift
//  Lesson2
//
//  Created by Clinton Jay Ramonida on 2017-07-12.
//  Copyright © 2017 Clinton Jay Ramonida. All rights reserved.
//

import Foundation

class ProductCategory {
    
    let categoryId: String!
    let name : String!
    let hasSubcategories: Bool!
    let productCount: Int!
    
    init(categoryId: String, name : String, hasSubcategories: Bool, productCount: Int) {
        
        self.categoryId = categoryId
        self.name = name
        self.hasSubcategories = hasSubcategories
        self.productCount = productCount
    }
}
