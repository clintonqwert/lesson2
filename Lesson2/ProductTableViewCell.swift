//
//  ProductTableViewCell.swift
//  Lesson2
//
//  Created by Clinton Jay Ramonida on 2017-07-19.
//  Copyright © 2017 Clinton Jay Ramonida. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var regularPriceLabel: UILabel!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var customerRatingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.00).cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
