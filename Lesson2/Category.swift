//
//  Category.swift
//  Lesson2
//
//  Created by Clinton Jay Ramonida on 2017-07-05.
//  Copyright © 2017 Clinton Jay Ramonida. All rights reserved.
//

import Foundation

class Category {
    
    let name: String!
    
    init (name:String) {
        self.name = name
    }
    
}
