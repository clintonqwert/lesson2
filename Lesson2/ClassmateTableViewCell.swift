//
//  ClassmateTableViewCell.swift
//  Lesson2
//
//  Created by Clinton Jay Ramonida on 2017-07-05.
//  Copyright © 2017 Clinton Jay Ramonida. All rights reserved.
//

import UIKit

class ClassmateTableViewCell: UITableViewCell {

    @IBOutlet weak var classmateCell: UILabel!
    @IBOutlet weak var classmateCellTwo: UILabel!
    
    override func awakeFromNib() {
        

        super.awakeFromNib()
    }
    
    func setupCell(withClassmate classmate: ClassMate)
    {
        classmateCell.text = classmate.name
        classmateCellTwo.text = classmate.country
    }

    
}
